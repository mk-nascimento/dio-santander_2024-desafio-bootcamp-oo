package edu.dio.bootcamp;

import java.time.LocalDate;
import java.util.Arrays;

import edu.dio.bootcamp.model.*;

public class App {
    public static void main(String[] args) {
        String separator = "-".repeat(12);
        String lineBreak = "\n";

        Course javaCourse = new Course();
        javaCourse.setTitle("Java Course");
        javaCourse.setDescription("Explore Java fundamentals in our 12-hour course.");
        javaCourse.setWorkload(12);

        Course springCourse = new Course();
        springCourse.setTitle("Spring Framework Course");
        springCourse.setDescription("Discover Spring Framework essentials");
        springCourse.setWorkload(4);

        Mentorship git = new Mentorship();
        git.setTitle("Git for Developers");
        git.setDescription("Empower developers with Git essentials");
        git.setDate(LocalDate.now().plusDays(2));

        Bootcamp bootcamp = new Bootcamp();
        bootcamp.setName("Santander 2024 - Backend com Java");
        bootcamp.setDescription("Boas-vindas à trilha de Back-end Java do Santander Bootcamp 2024!");
        bootcamp.getContents().addAll(Arrays.asList(javaCourse, springCourse, git));

        Dev john = new Dev();
        john.setName("John Doe");
        john.enrollBootcamp(bootcamp);
        System.out.println(john.getName() + " registered in: " + john.getRegistered()); // NOSONAR
        john.progress();
        john.progress();
        System.out.println(separator); // NOSONAR
        System.out.println(john.getName() + " registered in: " + john.getRegistered()); // NOSONAR
        System.out.println(john.getName() + " completed: " + john.getCompleted()); // NOSONAR
        System.out.println("XP: " + john.totalXp()); // NOSONAR

        System.out.println(lineBreak + separator + lineBreak); // NOSONAR

        Dev jane = new Dev();
        jane.setName("Jane Doe");
        jane.enrollBootcamp(bootcamp);
        System.out.println(jane.getName() + " registered in: " + jane.getRegistered()); // NOSONAR
        jane.progress();
        jane.progress();
        jane.progress();
        System.out.println(separator); // NOSONAR
        System.out.println(jane.getName() + " registered in: " + jane.getRegistered()); // NOSONAR
        System.out.println(jane.getName() + " completed: " + jane.getCompleted()); // NOSONAR
        System.out.println("XP: " + jane.totalXp()); // NOSONAR
    }
}
