package edu.dio.bootcamp.model;

import java.util.*;

public class Dev {
    private String name;
    private Set<Content> registered = new LinkedHashSet<>();
    private Set<Content> completed = new LinkedHashSet<>();

    public void enrollBootcamp(Bootcamp bootcamp) {
        this.registered.addAll(bootcamp.getContents());
        bootcamp.getDevelopers().add(this);
    }

    public void progress() {
        Optional<Content> content = this.registered.stream().findFirst();
        if (content.isPresent()) {
            this.completed.add(content.get());
            this.registered.remove(content.get());
        } else
            System.err.println("Você não possui conteúdos ainda em andamento!"); // NOSONAR
    }

    public double totalXp() {
        return this.completed.stream().mapToDouble(Content::getXp).sum();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Content> getRegistered() {
        return registered;
    }

    public void setRegistered(Set<Content> registered) {
        this.registered = registered;
    }

    public Set<Content> getCompleted() {
        return completed;
    }

    public void setCompleted(Set<Content> completed) {
        this.completed = completed;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((registered == null) ? 0 : registered.hashCode());
        result = prime * result + ((completed == null) ? 0 : completed.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Dev other = (Dev) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (registered == null) {
            if (other.registered != null)
                return false;
        } else if (!registered.equals(other.registered))
            return false;
        if (completed == null) {
            if (other.completed != null)
                return false;
        } else if (!completed.equals(other.completed))
            return false;
        return true;
    }

}
