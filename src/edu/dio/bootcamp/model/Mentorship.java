package edu.dio.bootcamp.model;

import java.time.LocalDate;

public class Mentorship extends Content {
    private LocalDate date;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public double getXp() {
        return DEFAULT_XP * 20D;
    }

}
