package edu.dio.bootcamp.model;

public class Course extends Content {
    private int workload;

    public int getWorkload() {
        return workload;
    }

    public void setWorkload(int workload) {
        this.workload = workload;
    }

    @Override
    public double getXp() {
        return DEFAULT_XP * workload;
    }
}
